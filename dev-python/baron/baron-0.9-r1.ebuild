# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{6,7,8,9,10,11} )
DISTUTILS_USE_PEP517="setuptools"

inherit distutils-r1

DESCRIPTION="Full Syntax Tree for python to make writing refactoring code a realist task"
HOMEPAGE="
	https://github.com/PyCQA/baron
	https://baron.readthedocs.io/en/latest/
"
SRC_URI="https://github.com/PyCQA/$PN/archive/$PV.tar.gz -> $P.gh.tar.gz"
LICENSE="LGPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="${PYTHON_DEPS}
	dev-python/rply[${PYTHON_USEDEP}]
"
RDEPEND="${DEPEND}"

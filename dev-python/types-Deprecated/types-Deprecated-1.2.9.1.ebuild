# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9..11} pypy3 )

DISTUTILS_USE_PEP517=setuptools
PYPI_NO_NORMALIZE=1
inherit distutils-r1 pypi

DESCRIPTION="Typing stubs for deprecated"
HOMEPAGE="
	https://pypi.org/project/types-Deprecated/
	https://github.com/python/typeshed/tree/master/stubs/Deprecated
"

SLOT="0"
LICENSE="Apache-2.0"
KEYWORDS="~amd64"

DEPEND="${RDEPEND}"

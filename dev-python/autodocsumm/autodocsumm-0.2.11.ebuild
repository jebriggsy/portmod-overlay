# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10,11} )
DISTUTILS_USE_PEP517="setuptools"

inherit distutils-r1

DESCRIPTION="Extended sphinx autodoc including automatic autosummaries"
HOMEPAGE="
	https://github.com/Chilipp/autodocsumm
"
SRC_URI="https://github.com/Chilipp/$PN/archive/refs/tags/v$PV.tar.gz -> $P.gh.tar.gz"
LICENSE="LGPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="${PYTHON_DEPS}
	dev-python/setuptools[${PYTHON_USEDEP}]
	dev-python/versioneer[${PYTHON_USEDEP}]
"
RDEPEND="dev-python/sphinx[${PYTHON_USEDEP}]"

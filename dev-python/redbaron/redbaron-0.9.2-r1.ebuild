# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{7,8,9,10,11} )
DISTUTILS_USE_PEP517="setuptools"

inherit distutils-r1

DESCRIPTION="Abstraction on top of baron, a FST for writing python refactoring code"
HOMEPAGE="
	https://github.com/PyCQA/redbaron
	https://redbaron.readthedocs.io/en/latest/
"
SRC_URI="https://github.com/PyCQA/$PN/archive/$PV.tar.gz -> $P.gh.tar.gz"
LICENSE="LGPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="${PYTHON_DEPS}
	>=dev-python/baron-0.7[${PYTHON_USEDEP}]
"
RDEPEND="${DEPEND}"

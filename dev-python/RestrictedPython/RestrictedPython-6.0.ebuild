# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{8,9,10,11} )
DISTUTILS_USE_PEP517="setuptools"

inherit pypi distutils-r1

MY_P=${P/_beta/b}
MY_PV=${PV/_beta/b}

DESCRIPTION="A subset of Python allowing program input into a trusted environment."
HOMEPAGE="
	https://github.com/zopefoundation/RestrictedPython
	https://pypi.org/project/RestrictedPython/
"
SRC_URI="$(pypi_sdist_url --no-normalize ${PN} ${PV})"

LICENSE="ZPL"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"
IUSE="test"

RDEPEND=""
DEPEND="${REDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
	test? (
		dev-python/pytest[${PYTHON_USEDEP}]
		dev-python/pytest-mock[${PYTHON_USEDEP}]
	)"

S="${WORKDIR}/${MY_P}"

RESTRICT="!test? ( test )"

python_test() {
	pytest -v -v || die
}

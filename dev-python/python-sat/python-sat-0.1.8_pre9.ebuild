# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{6,7,8,9,10,11} )
DISTUTILS_USE_PEP517="setuptools"

inherit pypi distutils-r1

MY_PV=${PV/_pre/.dev}

DESCRIPTION="A toolkit for SAT-based prototyping in Python"
HOMEPAGE="https://github.com/pysathq/pysat"
SRC_URI="$(pypi_sdist_url --no-normalize ${PN} ${MY_PV})"
LICENSE="MIT"
SLOT="0"
IUSE=""
KEYWORDS="~amd64 ~x86"

RDEPEND="${PYTHON_DEPS}
	sys-libs/zlib
"
DEPEND="${DEPEND}
	dev-python/six
"

MY_P=${P/_pre/.dev}

S=$WORKDIR/$MY_P

SOLVERS="['cadical', 'glucose30', 'glucose41', 'lingeling', 'maplechrono','maplecm', 'maplesat', 'minicard', 'minisat22', 'minisatgh']"

src_unpack() {
	default_src_unpack
	cd "$PN-$MY_PV"
	python <<< "
import inspect, sys, os
sys.path.insert(0, os.path.join(os.getcwd(), 'solvers/'))
import prepare

for solver in $SOLVERS:
	prepare.download_archive(prepare.sources[solver])
	prepare.extract_archive(prepare.sources[solver][-1], solver)
	"
}

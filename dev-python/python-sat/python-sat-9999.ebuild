# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{6,7,8,9,10,11} )
DISTUTILS_USE_PEP517="setuptools"

inherit git-r3 distutils-r1

DESCRIPTION="A toolkit for SAT-based prototyping in Python"
HOMEPAGE="https://github.com/pysathq/pysat"
EGIT_REPO_URI="https://github.com/pysathq/pysat.git"
LICENSE="MIT"
SLOT="0"
IUSE=""

RDEPEND="${PYTHON_DEPS}
	sys-libs/zlib
"
DEPEND="${DEPEND}
	dev-python/six
"

SOLVERS="[
	'cadical',
	'glucose30',
	'glucose41',
	'lingeling',
	'maplechrono',
	'maplecm',
	'maplesat',
	'minicard',
	'minisat22',
	'minisatgh'
]"

src_unpack() {
	git-r3_src_unpack
	cd "$P"
	python <<< "
import inspect, sys, os
sys.path.insert(0, os.path.join(os.getcwd(), 'solvers/'))
import prepare

for solver in $SOLVERS:
	prepare.download_archive(prepare.sources[solver])
	prepare.extract_archive(prepare.sources[solver][-1], solver)
	"
}

# Copyright 2019-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{10,11} )
DISTUTILS_USE_PEP517="setuptools"

inherit distutils-r1

DESCRIPTION="A CLI tool to manage mods for OpenMW"
HOMEPAGE="https://gitlab.com/portmod/importmod"
SRC_URI="https://gitlab.com/portmod/$PN/-/archive/v$PV/$PN-v$PV.tar.bz2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

PYTHON_REQ_USE="ssl,threads"

RDEPEND="${PYTHON_DEPS}
	games-util/portmod[${PYTHON_USEDEP}]
	dev-python/GitPython[${PYTHON_USEDEP}]
	dev-python/requests[${PYTHON_USEDEP}]
	dev-python/beautifulsoup4[${PYTHON_USEDEP}]
	app-arch/patool
	openmw? (
		games-util/tr-patcher
		games-util/tes3cmd
	)
	dev-python/PyGithub[${PYTHON_USEDEP}]
	dev-vcs/python-gitlab[${PYTHON_USEDEP}]
	dev-python/ruamel-yaml[${PYTHON_USEDEP}]
	dev-python/pypi-simple[${PYTHON_USEDEP}]
	dev-python/virtualenv[${PYTHON_USEDEP}]
	dev-python/tomlkit[${PYTHON_USEDEP}]
	dev-python/prompt-toolkit[${PYTHON_USEDEP}]
	>=dev-python/isort-5.0.0[${PYTHON_USEDEP}]
	dev-python/black[${PYTHON_USEDEP}]
	dev-python/fuzzywuzzy[${PYTHON_USEDEP}]
"

BDEPEND="${RDEPEND}
	test? (
		dev-python/pytest[${PYTHON_USEDEP}]
	)
	dev-python/setuptools-scm[${PYTHON_USEDEP}]
"

RESTRICT="!test? ( test )"

S="$WORKDIR/$PN-v$PV"
IUSE="test openmw"

export SETUPTOOLS_SCM_PRETEND_VERSION=$PV

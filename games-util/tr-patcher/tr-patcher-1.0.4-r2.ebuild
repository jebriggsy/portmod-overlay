# Copyright 2020-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit java-pkg-2 java-pkg-simple

DESCRIPTION="A modified, CLI version of the tr-patcher by the Tamriel Rebuilt Team."
HOMEPAGE="https://gitlab.com/bmwinger/tr-patcher"
SRC_URI="https://gitlab.com/bmwinger/$PN/-/archive/v$PV/$PN-v$PV.tar.bz2 -> $P.tar.bz2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="~dev-java/gradle-bin-7.5.1"
RDEPEND="${DEPEND}"
BDEPEND=""

S="$WORKDIR/$PN-v$PV"

src_compile() {
	TERM=xterm gradle-bin-7.5.1 build -g "$T" || die
}

src_install() {
	./install.sh "$D/usr" || die
}

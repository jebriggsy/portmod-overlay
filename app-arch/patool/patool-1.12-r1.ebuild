# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{7..11} )
DISTUTILS_USE_PEP517="setuptools"

inherit pypi distutils-r1

DESCRIPTION="Portable archive file manager"
HOMEPAGE="https://wummel.github.io/patool/"
PATCHES="${FILESDIR}/stripext.patch ${FILESDIR}/file-fallback.patch"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
